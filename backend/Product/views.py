from django.shortcuts import render, get_list_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from .models import Product
from .serializer import ProductSerializer, ProductImageSerializer

# Create your views here.

class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = get_list_or_404(Product)
    #permission_classes = [IsAuthenticated]

    @action(methods=['POST'], detail=True)
    def profile_update(self, request, pk=None):
        """To update save images associated to a product"""
        product = self.get_object()
        serializer = ProductImageSerializer(instance=product, data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)

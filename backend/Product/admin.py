from django.contrib import admin

from .models import Product, Image

# Register your models here.


class PostImageAdmin(admin.StackedInline):
    model = Image


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [PostImageAdmin]

    class Meta:
        model = Product


admin.site.register(Image)
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Product(models.Model):
    name = models.CharField(verbose_name='Name', max_length=60)
    price = models.FloatField(verbose_name='Price', blank=False, validators=[MinValueValidator(0)])
    rating = models.FloatField(verbose_name='Rating', blank=False, validators=[MaxValueValidator(5), MinValueValidator(1)])

    def __str__(self):
        return f'{self.name}'


class Image(models.Model):
    image = models.ImageField(default='Default.jpg', upload_to='products/images')
    album = models.ForeignKey(Product, related_name='product_image', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.album}'

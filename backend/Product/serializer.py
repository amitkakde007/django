from rest_framework import serializers

from .models import Product, Image


class ProductImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ['image', ]


class ProductSerializer(serializers.ModelSerializer):
    product_image = ProductImageSerializer(many=True)

    class Meta:
        model = Product
        fields = ('name', 'price', 'rating', 'product_image')

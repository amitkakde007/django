from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from .models import UserProfile, UserAddress


class UserSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(
        label='Confirm password',
        write_only=True,
        style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'confirm_password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password'
                }
            }
        }

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        return user

class UserLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username','password']
        extra_kwargs ={
            'password':{
                'write_only':True,
                'style':{
                    'input_type':'password',
                }
            }
        }


class UserAddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserAddress
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    address = UserAddressSerializer(many=True)

    class Meta:
        model = UserProfile
        fields = ['user', 'phoneno', 'address']

# Generated by Django 3.0.3 on 2020-12-26 08:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Authentication', '0009_auto_20201226_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='useraddress',
            name='pincode',
            field=models.CharField(default=0, max_length=6, verbose_name='Pin Code'),
        ),
    ]

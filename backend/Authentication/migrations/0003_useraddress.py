# Generated by Django 3.1.3 on 2020-11-26 07:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Authentication', '0002_auto_20201126_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAddress',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(default=' ', max_length=250, verbose_name='Address')),
                ('user_address', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Authentication.userprofile')),
            ],
        ),
    ]

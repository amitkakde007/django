from django.shortcuts import render,get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from rest_framework import viewsets, views
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import RetrieveAPIView
from rest_framework_simplejwt.tokens import RefreshToken



from .serializers import UserSerializer, UserProfileSerializer, UserAddressSerializer, UserLoginSerializer
from .models import UserProfile, UserAddress
from backend import settings

# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class LogoutAndBlackListToken(views.APIView):

    def post(self, request, *args, **kwargs):
        try:
            refreshtoken = request.data['refresh']
            token = RefreshToken(refreshtoken)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()


class UserAddressViewSet(views.APIView):
    serializer_class = UserAddressSerializer


    def get(self, request, *args, **kwargs):
        query = UserAddress.objects.all()
        serialize = self.serializer_class(query, many=True)
        return Response(data=serialize.data)


    def post(self, request, *args, **kwargs):
        serialize = self.serializer_class(data=request.data)
        if serialize.is_valid():
            serialize.save()
            return Response(data=serialize.data, status=status.HTTP_200_OK)
        else:
            return Response(serialize.errors, status=status.HTTP_400_BAD_REQUEST)

class UserAddressView(views.APIView):
    serializer_class = UserAddressSerializer
    
    def get(self, request, slug, **kwargs):
        user = get_object_or_404(User, username=slug)
        query = UserAddress.objects.filter(user_address=user.id)
        serialize = self.serializer_class(query, many=True)
        if serialize.data:
            return Response(data=serialize.data, status=status.HTTP_200_OK)
        else:
            return Response(serialize.error, status=status.HTTP_400_BAD_REQUEST)

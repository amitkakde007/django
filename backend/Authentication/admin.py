from django.contrib import admin


from .models import UserProfile, UserAddress
# Register your models here.

for value in [UserProfile, UserAddress]:
    admin.site.register(value)

from django.contrib.auth.models import User
from django.core.validators import RegexValidator,MaxLengthValidator
from django.db import models
# Create your models here.


class UserProfile(models.Model):
    phonevalidation = RegexValidator(regex=r'^(\+\d{1,3})?,?\s?\d{10}')
    phoneno = models.CharField(
        verbose_name='Mobile No',
        default=0000000000,
        max_length=10, unique=True,
        validators=[phonevalidation])

    user = models.OneToOneField(User, related_name='user', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'


class UserAddress(models.Model):
    address = models.CharField(verbose_name='Flat, House no., Building, Company, Apartment', default='', blank=False, max_length=100)
    landmark = models.CharField(verbose_name='Landmark', blank=True,max_length=30)
    city = models.CharField(verbose_name='City', default='', blank=False, max_length=30)
    state = models.CharField(verbose_name='State', default='', blank=False, max_length=30)
    pincode = models.CharField(verbose_name='Pin Code', blank=False, default=000000, max_length=6)
    user_address = models.ForeignKey(User, related_name='address', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user_address}'

"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))"""

from django.urls import path, include

from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from .views import UserViewSet, LogoutAndBlackListToken, UserProfileViewSet, UserAddressViewSet, UserAddressView

router = DefaultRouter()
router.register('register-viewset', UserViewSet, basename='register-user')
router.register('profile-viewset', UserProfileViewSet, basename='user-profile')


urlpatterns = [
    path('', include(router.urls)),
    path('api/token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('logout/', LogoutAndBlackListToken.as_view(), name='logout-delete-token'),
    path('address/', UserAddressViewSet.as_view(), name='address'),
    path('address/<slug:slug>/', UserAddressView.as_view(), name='user-address'),
]
